package br.com.devmedia.jpql;

import br.com.devmedia.jpql.consultas.ConsultasJPQL;

public class Programa {

	public static void main(String[] args) {
		ConsultasJPQL consultas = new ConsultasJPQL();
		
		System.out.println("\n------------------------ listarTodasAsSeries --------------------------\n");
		System.out.println(consultas.listarTodasAsSeries());
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ listarSeriesCriadasNoAno2008 --------------------------\n");
		System.out.println(consultas.listarSeriesCriadasNoAno2008());
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ listarSeriesCriadasNoAno --------------------------\n");
		System.out.println(consultas.listarSeriesCriadasNoAno(2013));
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ listarSeriesCujoNomeContenha --------------------------\n");
		System.out.println(consultas.listarSeriesCujoNomeContenha("ng"));
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ recuperarSeriePorAnoENome --------------------------\n");
		System.out.println(consultas.recuperarSeriePorAnoENome("Gotham", 2013));
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ recuperarNomeDaSeriePorAnoENome --------------------------\n");
		System.out.println(consultas.recuperarNomeDaSeriePorAnoENome("Gotham", 2013));
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ listarEpisodioDaSerie --------------------------\n");
		System.out.println(consultas.listarEpisodioDaSerie("Gotham"));
		System.out.println("\n------------------------------------------------------\n");
		
		System.out.println("\n------------------------ recuperarSeriesComNumeroMinimoDeTemporadas --------------------------\n");
		System.out.println(consultas.recuperarSeriesComNumeroMinimoDeTemporadas(3));
		System.out.println("\n------------------------------------------------------\n");
	}
	
}