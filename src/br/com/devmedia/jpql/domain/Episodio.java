package br.com.devmedia.jpql.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "episodios")
public class Episodio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private Integer numero;

	@Column(length = 30, nullable = false)
	private String titulo;

	@Column(length = 255)
	private String descricao;

	@ManyToOne(cascade = CascadeType.ALL)
	private Temporada temporada;

	public Episodio() {
		
	}

	public Episodio(Integer numero, String titulo, String descricao, Temporada temporada) {
		this.numero = numero;
		this.titulo = titulo;
		this.descricao = descricao;
		this.temporada = temporada;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Temporada getTemporada() {
		return temporada;
	}

	public void setTemporada(Temporada temporada) {
		this.temporada = temporada;
	}

	@Override
	public String toString() {
		return "Episodio [id=" + id +
				", numero=" + numero +
				", titulo=" + titulo +
				", descricao=" + descricao +
				", temporada=" + temporada + "]";
	}

}