package br.com.devmedia.jpql.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "temporadas")
public class Temporada {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Integer numero;

	@Column(length = 255, nullable = false)
	private String descricao;

	@ManyToOne(cascade = CascadeType.ALL)
	private Serie serie;

	@OneToMany(mappedBy = "temporada", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<Episodio> episodios;

	public Temporada() {
		
	}
	
	public Temporada(Integer numero, String descricao, Serie serie) {
		this.numero = numero;
		this.descricao = descricao;
		this.serie = serie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Serie getSerie() {
		return serie;
	}

	public void setSerie(Serie serie) {
		this.serie = serie;
	}

	public List<Episodio> getEpisodios() {
		return episodios;
	}

	public void setEpisodios(List<Episodio> episodios) {
		this.episodios = episodios;
	}

	@Override
	public String toString() {
		return "Temporada [id=" + id +
				", numero=" + numero +
				", descricao=" + descricao +
				", serie=" + serie + "]";
	}

}