package br.com.devmedia.jpql.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "series")
public class Serie {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 25, nullable = false)
	private String nome;
	private Integer anoCriacao;
	
	@Column(length = 255, nullable = false)
	private String descricao;
	
	@OneToMany(mappedBy = "serie", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<Temporada> temporadas;
	
	public Serie() {
		
	}
	
	public Serie(String nome, Integer anoCriacao, String descricao) {
		this.nome = nome;
		this.anoCriacao = anoCriacao;
		this.descricao = descricao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAnoCriacao() {
		return anoCriacao;
	}

	public void setAnoCriacao(Integer anoCriacao) {
		this.anoCriacao = anoCriacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return "Serie [id=" + id +
				", nome=" + nome +
				", anoCriacao=" + anoCriacao +
				", descricao=" + descricao + "]";
	}

}