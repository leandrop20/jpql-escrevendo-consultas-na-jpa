package br.com.devmedia.jpql.consultas;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.devmedia.jpql.domain.Episodio;
import br.com.devmedia.jpql.domain.Serie;
import br.com.devmedia.jpql.util.JPAUtil;

public class ConsultasJPQL {

	public List<Serie> listarTodasAsSeries() {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s FROM Serie s";
		
		return em.createQuery(jpql, Serie.class).getResultList();
	}
	
	public List<Serie> listarSeriesCriadasNoAno2008() {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s FROM Serie s WHERE s.anoCriacao = 2008";
		
		return em.createQuery(jpql, Serie.class).getResultList();
	}
	
	public List<Serie> listarSeriesCriadasNoAno(int ano) {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s FROM Serie s WHERE s.anoCriacao = :anoDesejado";
		
		return em.createQuery(jpql, Serie.class)
				.setParameter("anoDesejado", ano)
				.getResultList();
	}
	
	public List<Serie> listarSeriesCujoNomeContenha(String nome) {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s FROM Serie s WHERE s.nome LIKE :nome";
		
		return em.createQuery(jpql, Serie.class)
				.setParameter("nome", "%" + nome + "%")
				.getResultList();
	}
	
	public Serie recuperarSeriePorAnoENome(String nome, int ano) {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s FROM Serie s WHERE s.nome LIKE :nome AND S.anoCriacao = :ano"; 
		
		try {
			return em.createQuery(jpql, Serie.class)
					.setParameter("nome", "%" + nome + "%")
					.setParameter("ano", ano)
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public String recuperarNomeDaSeriePorAnoENome(String nome, int ano) {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s.nome FROM Serie s WHERE s.nome LIKE :nome AND S.anoCriacao = :ano";
		
		try {
			return em.createQuery(jpql, String.class)
					.setParameter("nome", "%" + nome + "%")
					.setParameter("ano", ano)
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Episodio> listarEpisodioDaSerie(String nome) {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT e FROM Episodio e WHERE e.temporada.serie.nome LIKE :nome";
		
		return em.createQuery(jpql, Episodio.class)
				.setParameter("nome", nome)
				.getResultList();
	}
	
	public List<Serie> recuperarSeriesComNumeroMinimoDeTemporadas(int temporadas) {
		EntityManager em = JPAUtil.getEm();
		
		String jpql = "SELECT s FROM Serie s JOIN S.temporadas t WHERE t.numero = :qtdTemporadas";
		
		return em.createQuery(jpql, Serie.class)
				.setParameter("qtdTemporadas", temporadas)
				.getResultList();
	}
	
}