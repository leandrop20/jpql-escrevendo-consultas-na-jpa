package br.com.devmedia.jpql.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory emf;
	
	public static EntityManager getEm() {
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory("CDM-jpql-escrevendo-consultas-na-jpa");
		}
		return emf.createEntityManager();
	}
	
}