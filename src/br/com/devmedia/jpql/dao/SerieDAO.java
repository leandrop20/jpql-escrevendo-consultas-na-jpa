package br.com.devmedia.jpql.dao;

import br.com.devmedia.jpql.domain.Serie;

public class SerieDAO extends GenericDAO<Serie> {

	public SerieDAO() {
		super(Serie.class);
	}
	
}