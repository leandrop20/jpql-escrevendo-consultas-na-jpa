package br.com.devmedia.jpql.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.devmedia.jpql.util.JPAUtil;

public abstract class GenericDAO<T> {
	
	private EntityManager em;
	private final Class<T> clazz;
	
	public GenericDAO(Class<T> clazz) {
		em = JPAUtil.getEm();
		this.clazz = clazz;
	}
	
	public void persist(T entity) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(entity);
		et.commit();
	}
	
	public void update(T entity) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.merge(entity);
		et.commit();
	}
	
	public void remove(T entity) {
		EntityTransaction et = em.getTransaction();
		et.begin();
		em.remove(entity);
		et.commit();
	}
	
	public List<T> list() {
		return em.createQuery("SELECT e FROM " + this.clazz.getName() + " e", this.clazz)
				.getResultList();
	}
	
	public T get(Long id) {
		try {
			return em.createQuery("SELECT e FROM " + this.clazz.getName() + " e", this.clazz)
					.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
}