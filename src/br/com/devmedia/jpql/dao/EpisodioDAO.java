package br.com.devmedia.jpql.dao;

import br.com.devmedia.jpql.domain.Episodio;

public class EpisodioDAO extends GenericDAO<Episodio> {

	public EpisodioDAO() {
		super(Episodio.class);
	}
	
}