package br.com.devmedia.jpql.dao;

import br.com.devmedia.jpql.domain.Temporada;

public class TemporadaDAO extends GenericDAO<Temporada> {

	public TemporadaDAO() {
		super(Temporada.class);
	}
	
}