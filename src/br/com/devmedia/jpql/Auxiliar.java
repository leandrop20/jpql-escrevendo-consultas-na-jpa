package br.com.devmedia.jpql;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.com.devmedia.jpql.dao.EpisodioDAO;
import br.com.devmedia.jpql.domain.Episodio;
import br.com.devmedia.jpql.domain.Serie;
import br.com.devmedia.jpql.domain.Temporada;
import br.com.devmedia.jpql.util.JPAUtil;

public class Auxiliar {

	public static void main(String[] args) {
		EntityManager em = JPAUtil.getEm();
		EntityTransaction et = em.getTransaction();
		
		et.begin();
		
		Serie serie1 = new Serie("Gotham", 2013, "Hist�ria do Batman");
		Serie serie2 = new Serie("The Walking Dead", 2008, "Trag�dia com zumbis");
		Serie serie3 = new Serie("Stranger Things", 2016, "Mist�rio sobre experimentos com crian�as");
		
		////////////////////////////////////////////////////////////////////////////////
		
		Temporada temporada1 = new Temporada(1, "O in�cio da hist�ria", serie1);
		Temporada temporada2 = new Temporada(2, "A continuidade da hist�ria", serie1);
		Temporada temporada3 = new Temporada(1, "O come�o de tudo", serie2);
		Temporada temporada4 = new Temporada(2, "Guerras e sobreviv�ncia", serie2);
		Temporada temporada5 = new Temporada(3, "Novos desafios", serie2);
		Temporada temporada6 = new Temporada(1, "Como tudo come�ou", serie3);
		
		////////////////////////////////////////////////////////////////////////////////
		
		Episodio episodio1 = new Episodio(1, "O come�o", "Primeiro epis�dio da s�rie", temporada1);
		Episodio episodio2 = new Episodio(2, "O segundo", "Segundo epis�dio da s�rie", temporada1);
		Episodio episodio3 = new Episodio(1, "O rein�cio", "Primeiro epis�dio da segunda temporada", temporada2);
		Episodio episodio4 = new Episodio(2, "O segundo", "Segundo ep�s�dio da segunda temporada", temporada2);
		
		Episodio episodio5 = new Episodio(1, "A trag�dia", "Come�ando a hist�ria", temporada3);
		Episodio episodio6 = new Episodio(2, "Contina��o da trag�dia", "Continuando a hist�ria", temporada3);
		Episodio episodio7 = new Episodio(1, "O retorno de Rick", "Rick retorna para o grupo", temporada4);
		Episodio episodio8 = new Episodio(1, "O milagre de Glenn", "Glenn derrota zumbis", temporada5);
		
		Episodio episodio9 = new Episodio(1, "Coisas estranhas", "A cria��o do mist�rio", temporada6);
		
		/*SerieDAO serieDAO = new SerieDAO();
		serieDAO.persist(serie1);
		serieDAO.persist(serie2);
		serieDAO.persist(serie3);
		
		/*TemporadaDAO temporadaDAO = new TemporadaDAO();
		temporadaDAO.persist(temporada1);
		temporadaDAO.persist(temporada2);
		temporadaDAO.persist(temporada3);
		temporadaDAO.persist(temporada4);
		temporadaDAO.persist(temporada5);
		temporadaDAO.persist(temporada6);*/
		
		EpisodioDAO episodioDAO = new EpisodioDAO();
		episodioDAO.persist(episodio1);
		episodioDAO.persist(episodio2);
		episodioDAO.persist(episodio3);
		episodioDAO.persist(episodio4);
		episodioDAO.persist(episodio5);
		episodioDAO.persist(episodio6);
		episodioDAO.persist(episodio7);
		episodioDAO.persist(episodio8);
		episodioDAO.persist(episodio9);
		
		em.close();
	}
	
}